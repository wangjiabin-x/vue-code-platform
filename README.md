
## 介绍

前端部分，推荐 `cnpm` 安装依赖。 

## 使用教程

### 安装
```
npm i
```

### 运行
```
npm run dev
```

### 打包
```
npm run build
```